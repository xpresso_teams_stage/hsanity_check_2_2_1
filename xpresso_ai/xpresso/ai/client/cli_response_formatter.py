""" Converts client response into a print statement"""

import re
from tabulate import tabulate
from IPython.display import display, HTML
from click import secho

from xpresso.ai.core.commons.utils import data_utils
from xpresso.ai.core.commons.utils.constants import OUTPUT_COLOR_GREEN, \
    OUTPUT_COLOR_RED

__all__ = ['CLIResponseFormatter']
__author__ = 'Naveen Sinha'

from xpresso.ai.core.data.visualization.utils import detect_notebook


class CLIResponseFormatter:
    """
    Takes a data and creates string in a specific format
    """
    # Key to print only value not key
    MESSAGE_KEY = "message"

    PRINT_FORMAT_HIERARCHICAL = "hierarchical"
    PRINT_FORMAT_EXPERIMENT_TABULAR = "experiment_tabular"

    OUTPUT_TYPE_PLAIN_TEXT = "plain"
    OUTPUT_TYPE_HTML = "html"

    def __init__(self):
        self.is_error = False
        self.output_type = self.OUTPUT_TYPE_PLAIN_TEXT

    @staticmethod
    def format_string(name: str):

        tmp_name = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
        underscore_name = re.sub('([a-z0-9])([A-Z])', r'\1_\2',
                                 tmp_name)
        updated_name = underscore_name.replace("_", " ")
        updated_name_list = []
        for word in updated_name.split(' '):
            if word.isupper():
                updated_name_list.append(word)
            else:
                updated_name_list.append(word.title())

        return ' '.join(updated_name_list)

    def print_to_console(self, message, is_highlight=False):
        """ Prints a message in aesthetic form

        Args:
            is_highlight(bool): Whether to highlight this message in the output.
                          Uses green color for highlight
            message(str): String message which needs to be printed to console
        """
        if self.is_error:
            secho(message=message, err=self.is_error, fg=OUTPUT_COLOR_RED)
        elif is_highlight:
            secho(message=message, err=self.is_error, fg=OUTPUT_COLOR_GREEN)
        else:
            secho(message=message)

    def print_new_line(self, value, item, index):
        """
        Print new line if required. After every line and dictionary, there
        should be a new line for better aesthetics
        Args:
            value: Value which is being used
            item: It is the parent container, May be a list or dictionary
            index:  Current Index

        """
        if isinstance(value, dict) and index != len(item) - 1:
            # This is being done to add a new line after a dict/list is
            # printed
            self.print_to_console(message="")

    def print_recursively(self, item, shift=0, is_highlight=False):
        """
        Prints an complex object to the console output. To be used for pretty
        printing It creates the string in a recursive loop.

        If any item has key as "message", it highlights the message

        Args:
            is_highlight(bool): Whether to highlight this message in the output.
            item: complex object
            shift(int): number of spaces to add before printing
        """
        if not item:
            return

        if isinstance(item, dict):
            if isinstance(item, dict):
                delete_keys = [key for key in item if not item[key]]
                for key in delete_keys:
                    del item[key]

            for index, key in enumerate(list(item.keys())):
                value = item[key]
                if ((isinstance(value, list) or isinstance(value, dict))
                    and key == self.MESSAGE_KEY):
                    self.print_recursively(value, shift, is_highlight=True)
                elif isinstance(value, list) or isinstance(value, dict):
                    self.print_to_console(
                        message=f"{shift * ' '}"
                                f"{self.format_string(key)}: ",
                        is_highlight=is_highlight)
                    self.print_recursively(value, shift + 2,
                                           is_highlight=is_highlight)
                elif key == self.MESSAGE_KEY:
                    self.print_to_console(message=f"{shift * ' '}{value}",
                                          is_highlight=True)
                else:
                    self.print_to_console(
                        message=f"{shift * ' '}"
                                f"{self.format_string(key)}: {value}",
                        is_highlight=is_highlight)

                self.print_new_line(value=value, item=list(item.keys()),
                                    index=index)

        elif isinstance(item, list):
            for index, value in enumerate(item):
                self.print_recursively(value, shift, is_highlight=is_highlight)
                self.print_new_line(value=value, item=item, index=index)

        else:
            self.print_to_console(message=f"{shift * ' '}{item}",
                                  is_highlight=is_highlight)

    def print_multiple_tables(self, data):
        """ Print the data in tabular format
        It works on dict/list data. It takes top 2 level in the dictionary
        as column name and creates row from their value in all the rest of
        the dictionary
        """
        if not isinstance(data, list):
            return

        for item in data:
            print_df = data_utils.generate_single_table_df(data=item, max_depth=2)
            if self.output_type == self.OUTPUT_TYPE_HTML:
                html_data = tabulate(print_df, headers="keys", tablefmt="html",
                                     showindex="never").replace("\n", " ")
                if detect_notebook():
                    display(HTML(html_data))
                else:
                    print(html_data)
            else:
                print(tabulate(print_df, headers="keys", tablefmt="grid",
                               showindex="never").replace("\n", " "))

    def pretty_print(self, data, format_type=PRINT_FORMAT_HIERARCHICAL,
                     is_error=False, output_type=OUTPUT_TYPE_PLAIN_TEXT):
        """ Generate print statement in the specified format """
        self.is_error = is_error
        self.output_type = output_type
        if format_type == self.PRINT_FORMAT_EXPERIMENT_TABULAR:
            return self.print_multiple_tables(data)
        return self.print_recursively(data)


if __name__ == "__main__":
    test_data = {"result": {"message": "empty_list, empty_str & "
                                       "has-empty-ele.id2 shouldn't be printed",
                            "build successful": [{"id1": 1}, {"id2": 2}],
                            "Test successful": [1, 2, 3, 4],
                            "Should be s": ["1", "2", "3"],
                            "empty_list": [],
                            "empty_str": '',
                            "has-empty-ele": [{"id1": 1}, {"id2": None}]
                            },
                 "hyper": "temp",
                 "Testing": "Asd"
                 }
    CLIResponseFormatter().pretty_print(data=test_data)
    CLIResponseFormatter().pretty_print(data=test_data, is_error=True)
